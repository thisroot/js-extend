var express = require('express');
var app = express();
var db  = require('./db.js');
var bodyParser = require('body-parser');

var birds = require('./routes/birds');


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use('/birds', birds);

db.migrate.latest().then(() => {
    app.listen(3000, () => console.log('Example app listening on port 3000!'))
});