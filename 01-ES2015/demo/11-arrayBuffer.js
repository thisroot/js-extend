var buf1 = new ArrayBuffer(16),
buf2 = buf1.slice();

console.log(buf1.byteLength, buf2.byteLength); //16, 16
console.log("\n=====================\n")

var buffer = new ArrayBuffer(4),
u16 = new Uint16Array(buffer);

u16[0] = 0xFF;
console.log(u16);
console.log("\n=====================\n")

var buf = new ArrayBuffer( 2 );
var view8 = new Uint8Array( buf );
var view16 = new Uint16Array( buf );
view16[0] = 3085;
console.log(view8[0]); // 13
console.log(view8[1]); // 12
console.log(view8[0].toString( 16 )); // "d"
console.log(view8[1].toString( 16 )); // "c"
console.log(view16); // Uint16Array { 0: 3085 }
console.log(view16[0].toString(16)); // c0d
console.log("\n=====================\n")


var a = new Int32Array(3);
a[0] = 10;
a[1] = 20;
a[2] = 30;
a.map((v) => console.log(v)); // 10 20 30
console.log(a.join( "-" )); // "10-20-30"
console.log("\n=====================\n")