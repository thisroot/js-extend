function* generateSequence() {
    yield 1;
    yield 2;
    yield 3;
    return 4;
}

{
    // генератор имеет метод next(), который вызывает генератор до следующей точки
    let generator = generateSequence();
    console.log(generator.next());
    console.log(generator.next());
    console.log(generator.next());
    console.log(generator.next());
}

{
    let generator = generateSequence();
    //в случае перебора через for..of значение в return не возвращается
    for(let value of generator) {
        console.log(value); // 1, затем 2
    }
}


{
    function *foo() {
        while (true) {
            yield Math.random();
        }
    }

    let randomNum = foo();
    for(let i = 0; i<5;++i) {
        console.log(randomNum.next().value)
    }

}

//композиция генераторов

{
    function* generateSequence(start, end) {
        for (let i = start; i <= end; i++) yield i;
    }

    function* generateAlphaNum() {

        // 0..9
        //Здесь использована специальная форма yield*. Она применима //только к другому генератору и делегирует ему выполнение.
        // все yield внутреннего генератора устанавливаются на выполнение
        yield* generateSequence(48, 57);

        // A..Z
        yield* generateSequence(65, 90);

        // a..z
        yield* generateSequence(97, 122);
    }

    let str = '';

    for(let code of generateAlphaNum()) {
        str += String.fromCharCode(code);
    }

    console.log(str); // 0..9A..Za..z
}

{
    function *foo() {
        var x = yield 10;
        console.log(x);
        yield x*4;
    }

    var gen = foo();

    console.log(gen.next()); //{ value: 10, done: false }
    
    //переданное значение заменяет результат первого yield
    console.log(gen.next(120)); // 120 { value: 480, done: false }
}