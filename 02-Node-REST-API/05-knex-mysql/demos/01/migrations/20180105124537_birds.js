
exports.up = function(knex, Promise) {
    
            return Promise.all([
                knex.schema.createTable('birds', function(table) {
                    table.increments('uid').primary();
                    table.integer('birdTypeId')
                        .references('typeBirdsId')
                        .inTable('typeBirds')
                    table.string('location');
                    table.timestamps();
                }),
    
                knex.schema.createTable('typeBirds', function(table){
                    table.increments('typeBirdsId').primary();
                    table.string('title');
                    table.string('description');
                })
            ])
        };
    
exports.down = function(knex, Promise) {
        return Promise.all([
        knex.schema.dropTable('birds'),
        knex.schema.dropTable('typeBirds')
    ])
};
